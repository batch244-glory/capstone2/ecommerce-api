const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
const Product = require("../models/Product");

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {
		if (result.length > 0) {
			return true;
		} else {
			return false;
		}
	})
};


// Capstone step 1 - user registration & check email if already existing
module.exports.registerUser = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		
		let newUser = new User(
				{
					// firstName : reqBody.firstName,
					// lastName : reqBody.lastName,
					email : reqBody.email,
					mobileNo : reqBody.mobileNo,
					password : bcrypt.hashSync(reqBody.password, 10)
				}
			)
			return newUser.save().then((user, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			});
		
	})
}

// Capstone step 2 - user authentication (login)
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return false, "error logging in";
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect) {
				return {access : auth.createAccesToken(result)}
			} else {
				return false, "error logging in";
			}
		}
	})
};

// step 8 - user create order

module.exports.checkout = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orders.push({productId : data.productId});

		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})
	});

	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.orders.push({orderId : data.userId})

		return product.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})
	});

	if (isUserUpdated && isProductUpdated) {
		return true;
	} else {
		return false;
	}
}

// step 9 - retrieve user details

module.exports.getProfile = (reqBody) => {

	return User.findById(reqBody.userId).then(result => {
		result.password = "";
		return result;
	});
};


// Stretch goal 1 - set user as admin (admin only)
module.exports.setAdmin = (reqParams, reqBody) => {
	let updatedUserAsAdmin = {
		isAdmin : reqBody.isAdmin
	}
	return User.findByIdAndUpdate(reqParams.userId, updatedUserAsAdmin).then((user, error) =>{
		if(error){
			return false;
		} else{
			return true;
		}
	})
};

// Stretch goal 2 - retrieve authenticated user's orders
module.exports.getUserOrders = (reqBody) => {
	return User.findById(reqBody.userId).then(result => {
		return result.orders
	})
};

// Stretch goal 3 - retrieve all orders (admin only)
module.exports.getAllOrders = (reqBody) => {
	return User.find({}).then(result => {
		return result.orders
	})
}

// Stretch goal 3 - add to cart
module.exports.addtoCart = async (data) => {
	let isAddedToCart = await User.findById(data.userId).then(user => {
		user.cart.push({productId : data.productId});

		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true, "product added to cart";
			}
		})
	})
}


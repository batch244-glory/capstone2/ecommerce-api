const Product = require("../models/Product");

// Capstone step 3 - create product (admin only)
module.exports.createProduct = (data) => {
	let newProduct = new Product ({
		name: data.product.name,
		description: data.product.description,
		price: data.product.price
	});

	return newProduct.save().then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
};

// Capstone step 4 - retrieve all active products
module.exports.getAllActive = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	})
};

module.exports.getAllProducts = () => {
  return Product.find().then(result => {
    return result;
  });
};


// step 5 - retrieve single product
module.exports.getProduct = (data) => {
	return Product.findById(data.productId).then(result => {
		return result;
	})
}

// step 6 - update product info (Admin only)

module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
};

// step 7 - archive product (admin only)

module.exports.archiveProduct = (reqParams, reqBody) => {
	let updateActiveField = {
		isActive : reqBody.isActive
	}

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((course, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}
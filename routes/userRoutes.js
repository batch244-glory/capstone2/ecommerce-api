const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth")

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(
		resultFromController => res.send(resultFromController));
});
// user registration & check email if already existing
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(
		resultFromController => res.send(resultFromController))
});
// user authentication (login)
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})
// user create order (non admin)
router.post("/checkout", auth.verify, (req, res) => {
	const verify = auth.decode(req.headers.authorization);

	let data = {
		userId: verify.id,
		productId: req.body.productId
	}
	if (verify.isAdmin){
		res.send(false)
	} else {
		userController.checkout(data).then(resultFromController => res.send(resultFromController))
	}
	
});
// retrieve user details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId : userData.id}).then(resultFromController => res.send (resultFromController))
});
// set user as admin (admin only)
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	const data = auth.decode(req.headers.authorization)
	if (data.isAdmin) {
		userController.setAdmin(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false)
	}
})
// retrieve authenticated user's orders
router.get("/myOrders", auth.verify, (req, res) => {
	const data = auth.decode(req.headers.authorization);
	userController.getUserOrders({userId : data.id}).then(resultFromController => res.send(resultFromController))
})

router.get("/orders", auth.verify, (req, res) => {
	const data = auth.decode(req.headers.authorization)

	if (data.isAdmin) {
		userController.getAllOrders().then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false)
	}
})
// SG - add to cart
router.post("/myCart", auth.verify, (req, res) => {
	const verify = auth.decode(req.headers.authorization);

	let data = {
		userId: verify.id,
		productId: req.body.productId
	}
	if (verify.isAdmin){
		res.send(false)
	} else {
		userController.addtoCart(data).then(resultFromController => res.send(resultFromController))
	}
	
});

module.exports = router;


const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController")
const auth = require("../auth")


// create product
router.post("/create", auth.verify, (req, res) =>{
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if (data.isAdmin){
		productController.createProduct(data).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false)
	}
});
// retrieve all active products
router.get("/activeProducts", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController))
});

router.get("/allProducts", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
}); 
// retrive single product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
})
// update product info (Admin only)
router.put("/:productId/edit", auth.verify, (req, res) => {
	
	const data = auth.decode(req.headers.authorization)

	if (data.isAdmin){
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false)
	}
})
// archive product (admin only)
router.patch("/:productId/archive", auth.verify, (req, res) => {
	const data = auth.decode(req.headers.authorization)

	if(data.isAdmin){
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false)
	}
});

module.exports = router;